var M;
var prezzoOrario;
var prezzoKm;

var prezzoOrarioAssociati;
var prezzoKmAssociati;

var oreGratisAssociati;
var kmGratisAssociati;

var scontoDoblo;
var tariffario = {};
var sconti = {};

function init() {
    M = 30;
    prezzoOrario = 15;
    prezzoKm = 1.30;
    scontoDoblo = 40;

    prezzoOrarioAssociati = 15;
    prezzoKmAssociati = prezzoKm/2;

    oreGratisAssociati = 8;
    kmGratisAssociati = 400;

    // Per tutti gli utenti
    for (_utente of document.getElementById("utenza").options) {
        let utente = _utente.value;
        
        tariffario[utente] = {};

        tariffario[utente]["T1"] = {};
        tariffario[utente]["T1"]["Z1"] = "M";
        tariffario[utente]["T1"]["Z2"] = "MK";
        tariffario[utente]["T1"]["Z3"] = "MK";
        tariffario[utente]["T1"]["Z4"] = "MK";
        tariffario[utente]["T1"]["Z5"] = "MK";

        tariffario[utente]["T2"] = {};
        tariffario[utente]["T2"]["Z1"] = "MO";
        tariffario[utente]["T2"]["Z2"] = "MOK";
        tariffario[utente]["T2"]["Z3"] = "MK";
        tariffario[utente]["T2"]["Z4"] = "MK";
        tariffario[utente]["T2"]["Z5"] = "MK";

        tariffario[utente]["T3"] = {};
        tariffario[utente]["T3"]["Z1"] = "MO";
        tariffario[utente]["T3"]["Z2"] = "MOK";
        tariffario[utente]["T3"]["Z3"] = "MK";
        tariffario[utente]["T3"]["Z4"] = "MK";
        tariffario[utente]["T3"]["Z5"] = "MK";

        tariffario[utente]["T4"] = {};
        tariffario[utente]["T4"]["Z1"] = "MO";
        tariffario[utente]["T4"]["Z2"] = "MOK";
        tariffario[utente]["T4"]["Z3"] = "MK";
        tariffario[utente]["T4"]["Z4"] = "MKV";
        tariffario[utente]["T4"]["Z5"] = "MKV";

        tariffario[utente]["T5"] = {};
        tariffario[utente]["T5"]["Z1"] = "MOV";
        tariffario[utente]["T5"]["Z2"] = "MOKV";
        tariffario[utente]["T5"]["Z3"] = "MKV";
        tariffario[utente]["T5"]["Z4"] = "MKV";
        tariffario[utente]["T5"]["Z5"] = "MKVH";
    }

    // Per utenti AS e AA
    for (t of ["T1", "T2", "T3", "T4", "T5"]) {
        for (z of ["Z1", "Z2", "Z3", "Z4", "Z5"]) {
            tariffario["AS"][t][z] = "K";
            tariffario["AA"][t][z] = "K";
        }
    }
    tariffario["AS"]["T5"]["Z5"] = "AKVH";
    tariffario["AA"]["T5"]["Z5"] = "AKVH";

    // Sconti
    sconti["PR"] = 0;
    sconti["PH"] = 20;
    sconti["AS"] = 100;
    sconti["AA"] = 100;
    sconti["AB"] = 50;

    document.getElementById("boxRisultato").style = "display: none;";
    document.getElementById("zona").selectedIndex = 0;
    document.getElementById('ambulanza').checked = true;
    document.getElementById("utenza").selectedIndex = 0;
    document.getElementById("km").value = 0;
    document.getElementById("autostrada").value = 0;
    document.getElementById("vitto").value = 0;
    document.getElementById("hotel").value = 0;
    document.getElementById("ore").value = 0;
    
    attiva();
}

function calcola() {
    let totaleScontabile = 0;
    let totaleNonScontabile = 0;
    let totale = 0;
    let percentualeSconto = 0;
    let Z = document.getElementById("zona").value;
    let mezzo = document.querySelector('input[name="mezzo"]:checked').value;
    let utente = document.getElementById("utenza").value;

    let K = document.getElementById("km").value;
    let A = document.getElementById("autostrada").value;
    let V = document.getElementById("vitto").value;
    let H = document.getElementById("hotel").value;
    let O = document.getElementById("ore").value;

    let T = "T" + Math.min(5, Math.max(1, Math.ceil(O / 2)));

    //console.log(Z + " " + T);

    if ( mezzo == "doblo" ) {
        percentualeSconto = scontoDoblo;
    }
    
    if ( tariffario[utente][T][Z].includes("M") ) {
        totaleScontabile += 1* M;
    }

    if ( tariffario[utente][T][Z].includes("O") ) {
        totaleScontabile += 1* Math.min(120, (O - 2) * prezzoOrario);
    }

    if ( tariffario[utente][T][Z].includes("K") ) {
        totaleScontabile += 1* K * prezzoKm;
    }

    if ( tariffario[utente][T][Z].includes("V") ) {
        totaleNonScontabile += 1* V;
    }

    if ( tariffario[utente][T][Z].includes("H") || (Z === "Z4" && T == "T5" && O > 23) ) {
        totaleNonScontabile += 1* H;
    }

    if ( tariffario[utente][T][Z].includes("A") ) {
        totaleNonScontabile += 1* A;
    }

    if (utente == "AA" || utente == "AS") {
        if ( K > kmGratisAssociati) {
            totaleNonScontabile += 1* (K-kmGratisAssociati)*prezzoKmAssociati;
        }
        if ( O > oreGratisAssociati ) {
            totaleNonScontabile += 1* (O-oreGratisAssociati) * prezzoOrarioAssociati;
        }
    }

    percentualeSconto = Math.max(percentualeSconto, sconti[utente]);

    totale = totaleScontabile * (100 - percentualeSconto) / 100 + totaleNonScontabile;

    document.getElementById("boxRisultato").style = "";
    document.getElementById("risultato").innerHTML = totale;
}


function attiva() {
    let Z = document.getElementById("zona").value;
    let utente = document.getElementById("utenza").value;
    let O = document.getElementById("ore").value;
    let T = "T" + Math.min(5, Math.max(1, Math.ceil(O / 2)));

    if ( tariffario[utente][T][Z].includes("K") ) {
        document.getElementById("K").style = "";
    } else {
        document.getElementById("K").style = "display: none;";
    }

    if ( tariffario[utente][T][Z].includes("V") ) {
        document.getElementById("V").style = "";
    } else {
        document.getElementById("V").style = "display: none;";
    }

    if ( tariffario[utente][T][Z].includes("H") || (Z === "Z4" && T == "T5" && O > 23) ) {
        document.getElementById("H").style = "";
    } else {
        document.getElementById("H").style = "display: none;";
    }

    if ( tariffario[utente][T][Z].includes("A") ) {
        document.getElementById("A").style = "";
    } else {
        document.getElementById("A").style = "display: none;";
    }
}
